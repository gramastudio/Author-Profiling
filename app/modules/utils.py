import xml.etree.cElementTree as ET
from bs4 import BeautifulSoup

from nltk import sent_tokenize
from nltk.util import ngrams
import nltk
import sys

postagList = set([])

# Fuction: load the files
# Return the file's text

def read_file(file):
    tree = ET.parse(file)
    root = tree.getroot() 
    root_tag = root.tag

    gender = root.attrib['gender'] 
    lang = root.attrib['lang'] 
    age = root.attrib['age_group']

    textArray = []

    for conversation in root.findall("./conversations/conversation"):

        text = conversation.text
        if not text:
            continue
        text = text.replace("\t","")
        text = text.replace("\n","")
        soup = BeautifulSoup(text, "lxml")
        textArray.append(soup.get_text())

    return [gender,age],textArray


# Function: Text tokenization
# Split the text in words, and obtains the POS tag of each one
# Also, delete the invalid characters or those wich are not useful for the syntax analysis

def tokenize_text(text):
    sentences = sent_tokenize(text)
    syntaxArray = []
    
    for sentence in sentences:
        tokens = nltk.word_tokenize(sentence)
        tagged = nltk.pos_tag(tokens)
        synVector = []
        for tag in tagged:
            syntax = tag[1].replace('$','')
            if not(syntax in [',', '.',':', '\'\'', '``','(',')'] ):
                synVector.append(syntax)
                postagList.add(syntax)
        syntaxArray.append(synVector)
    return syntaxArray

def progress(bars):
    bar_len = 20
    string_arr = []
    for item in bars:
        count = bars.get(item).get('done')
        total = bars.get(item).get('total')
        name = bars.get(item).get('name')
        if total == 0:
            total = 0.0001
        filled_len = int(round(bar_len * count / float(total)))

        percents = round(100.0 * count / float(total), 1)
        bar = '=' * filled_len + '-' * (bar_len - filled_len)
        string_arr.append('[%s] %s%s -%s' % (bar, percents, '%', name))
   
    sys.stdout.write('\t'.join(string_arr)+'\r')
    sys.stdout.flush()  # As suggested by Rom Ruben