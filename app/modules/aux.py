# Function: Extract random lines from texts for graph validation

def RandomLinesExtractor(minlength, maxlength):
    
    print("\nExtracting random lines...")
    total = len(booksTest)
    iter = 1
    for book in booksTest:
        start_time = time.clock()
        print("loading: "+str(iter)+"/"+str(total)+"\r", end='')
        iter += 1
        data,text = read_file(booksdir+book)
        author = data[0]+"_"+data[1]
        sentences = sent_tokenize(text[0])
        totalLines = len(sentences)
        line = ""
        rnd = 0
        minlen = round(totalLines/10)
        rnd = random.randint(minlen, totalLines) - minlen
        for i in range(minlen):
            line += sentences[rnd + i]+" "
        line= line.split()
        textline= ' '.join(line)
        aux=[author, textline]
        randomLines.append(aux)
    print("Total lines: "+str(len(randomLines)))


# Function: Validates that the test text is a sub-graph of any of the graphs created.
# If actually is a sub-graph, it means that the text is written with the same syntactic style.

import numpy as np

def Validation():
    print("\nValidating graphs...")
    
    #F= open("results.txt","w")
    start_time = time.clock()
    
    y_pred.clear()
    y_true.clear()
    
    evalu = [0,0]
    booksArray = []

    
    Matrix = [[0 for x in range(len(authors))] for y in range(len(authors))]
    booksArray = [[0,0] for x in range(len(authors))]
    total = len(randomLines)
    iter = 1
    for k in range(len(randomLines)):
        print("loading: "+str(iter)+"/"+str(total)+"\r", end='')
        iter += 1
        #F.write(randomLines[k][1])                      #writes the text in a file
        ValidationGraph = process(randomLines[k][1])    #tokenize and process the string
        booksNumber = [0 for x in range(len(authors))]
        author = authors.index(randomLines[k][0])
        cont = 0
        for i in graphs.keys():
            authorindex = authors.index(i)
            for j in range(len(graphs[i])):             # j is the number of ngrams to analize
                G1 = graphs[i][j][1]                    #tooks a graph
                for n in range(len(ValidationGraph)):
                    G2 = ValidationGraph[n][1].edges()  #tooks the test graph
                    for v1, v2 in G2:
                        if(G1.has_edge(v1,v2)):
                            booksNumber[authorindex] +=G1[v1][v2]["weight"]
        maxval = max(booksNumber)
        index = booksNumber.index(maxval)

        Matrix[author][index] += 1
        
        y_pred.append(index)
        y_true.append(author)
        
        if(index == author):
            evalu[0] += 1
            booksArray[index][0] += 1
        else:
            evalu[1] += 1
            booksArray[index][1] += 1
    print("Total time: "+str(round(time.clock()-start_time,2)), "seconds")

    print("\n-----Results-----\n" )
    print("positives/negatives: "+ str(evalu))
        
    print(np.matrix(Matrix))
    return evalu

# Function: Return the scores evaluating the y_true and y_pred vectors

def check():
    print("\nValidating graphs...")
    
    start_time = time.clock()
    booksArray = []
    
    authors.sort()

    Matrix = [[0 for x in range(len(authors))] for y in range(len(authors))]
    booksArray = [[0,0] for x in range(len(authors))]
    total = len(randomLines)
    iter = 1
    for k in range(len(randomLines)):
        print("loading: "+str(iter)+"/"+str(total)+"\r", end='')
        iter += 1
        ValidationGraph = process(randomLines[k][1])    #tokenize and process the string
        rank = [0 for x in range(len(authors))]
        author = authors.index(randomLines[k][0])
        cont = 0
        for n in range(len(ValidationGraph)):
            G2 = ValidationGraph[n][1].edges()  #tooks the test graph
            booksNumber = [0 for x in range(len(authors))]
            nodeWeights = [0 for x in range(len(authors))]
            for i in graphs.keys():
                authorindex = authors.index(i)
                #print("checking:"+i)
                for j in range(len(graphs[i])):             # j is the number of ngrams to analize
                    G1 = graphs[i][j][1]                    #tooks a graph
                    for v1, v2 in G2:
                        if(G1.has_edge(v1,v2)):
                            booksNumber[authorindex] +=G1[v1][v2]["weight"]
                    for v in G2:
                        if(G1.has_node(v)):
                            print(G1.node[v]['weight'] )
                            nodeWeights[authorindex] +=G1.node[v]['weight'] 
            val = booksNumber[author]

            seq = sorted(booksNumber, reverse=True)
            orden = [seq.index(v) for v in booksNumber]
            for o in range(len(orden)):
                rank[o] += orden[o]

            maxval = max(booksNumber)
            if maxval > 0:
                index = booksNumber.index(maxval)
                booksNumber.sort(reverse=True)

            
    print("Total time: "+str(round(time.clock()-start_time,2)), "seconds")        
    

# Function: Return the scores evaluating the y_true and y_pred vectors

def Scores():
    print("\n-----Scores-----\n")
    print(" accuracy_score:",accuracy_score(y_true, y_pred))
    print("       f1_score:",f1_score(y_true, y_pred, average=None))
    print("precision_score:",precision_score(y_true, y_pred, average=None))