import networkx as nx
from modules.utils import *
import time
import math
import os.path
from os import listdir
from os.path import isfile, join

from collections import defaultdict
from operator import itemgetter

import re

# Function: Conversion from text to graph

def tokenize(line, ngrams):

    ngrams_list = []
 
    for num in range(0, len(line)):
        ngram = ' '.join(line[num:num + ngrams])
        ngrams_list.append(ngram)
 
    return ngrams_list

def toGraph(text, G, ngrams):

    ngrams = int(ngrams)
    for line in text:
        aux = ''
        if(len(line)>= ngrams):
            xgrams = tokenize(line,ngrams)
            for a in xgrams:
                if not G.has_node(a):
                    G.add_node(a, weight=1)
                else:
                    G.nodes[a]['weight'] += 1
                if(aux != ''):
                    if(G.has_edge(aux,a)):
                        G[aux][a]["weight"] += 1
                    else:
                        G.add_edge(aux,a,weight = 1)
                aux = a
    return G

def GraphGenerator(texts, author, ngrams, path, iter, total):
    filename = path+"/it"+str(iter).zfill(2)+"_ng"+str(ngrams).zfill(3)+"_nb"+ str(total).zfill(3)+ "_"+ author+".gefx"

    if os.path.isfile(filename):
        G = nx.read_gexf(filename)

    else:
        G=nx.DiGraph()
    
    for text in  texts:
        tokens = tokenize_text(text) 
        G = toGraph(tokens, G, ngrams)
        
        highedge = 0
        highnode = 0
        for v1, v2 in G.edges():
            if G[v1][v2]["weight"] > highedge:
                highedge = G[v1][v2]["weight"]
        for v in G:
            if G.node[v]["weight"] > highnode:
                highnode = G.node[v]["weight"]

        for v1, v2 in G.edges():
            G[v1][v2]["weight"] /= highedge

        for v in G:
            G.node[v]["weight"] /= highnode

    nx.write_gexf(G, filename)
    G.clear()



def func(v, fun,maxi):
        if fun == "s1":
            return 1 / (1 + math.exp(-2*v))
        elif fun == "s2":
            return 1 / (1 + math.exp(-v))
        elif fun == "s3":
            return 1 / (1 + math.exp(-v/2)) # plot 1/(1+e^(-x/10)) from x=-100 to 100
        elif fun == "s4":
            return 1 / (1 + math.exp(-v/3))
        elif fun == "s5":
            return 1 / (1 + math.exp(-v/maxi))
        elif fun == "v1":
            return math.fabs(math.erf((math.sqrt(math.pi)/2)*v))
        elif fun == "v2":
            return math.fabs(math.tanh(v))
        elif fun == "v3":
            return math.fabs(v/math.sqrt(1 + v**2))
        elif fun == "v4":
            return math.fabs((2/math.pi) * math.atan((math.pi/2)*v))
        else:
            return v

def processGraph(path, file, type, it, ntrain):
    logfile = "logfile.csv"
    log = open(logfile, "a") 
    

    if not os.path.exists(path+'group'):
        os.makedirs(path+'group')

    newpath = path+'/group/'+str(type)+'_T_'+str(ntrain)+".gexf"

    log.write(newpath +", "+file + "\n")
    log.close()
    if not os.path.isfile(newpath):
        G = nx.read_gexf(path + file)
    else:
        G = nx.read_gexf(newpath)
        g = nx.read_gexf(path + file)

        for node in list(g.nodes):
            if not G.has_node(node):
                G.add_node(node, weight=1)
            else:
                G.node[node]['weight'] += 1
        for edge in list(G.edges):
            if not G.has_edge(edge[0], edge[1]):
                G.add_edge(edge[0], edge[1],weight = 1 )
            else:
                G[edge[0]][edge[1]]["weight"] += 1

    nx.write_gexf(G, newpath)
    G.clear() 



def SingleGraphToGraph(it, ntrain):
    path = 'results/graph/iter'+str(it)+'/train/'
    folders = [f for f in listdir(path) if not isfile(join(path, f))]
    for folder in folders:
        test_path = path + folder + "/"
        print(test_path)
        files = [f for f in listdir(test_path) if isfile(join(test_path, f))]
        print(str(len(files))+ " "+ str(ntrain))
        counter = 0
        for file in files:
            print(file)
            if file != '.DS_Store':
                result = re.search('s(.+)_a', file)
                sex = result.group(1)
                result = re.search('_a(.+)s', file)
                age = result.group(1)

                if(counter < ntrain):
                    processGraph(test_path,file, "S_"+sex, it, ntrain)
                    processGraph(test_path,file, "A_"+age, it, ntrain)
                    processGraph(test_path,file, "B_"+age+"_"+sex, it, ntrain)
                else:
                    counter = 0
                    break
                counter += 1
                

def eval_subgraph(trainGraph, testGraph, f):

    authors = trainGraph.keys()
    authors = list(authors)
    
    EdgesDict = defaultdict(float)
    NodesDict = defaultdict(float)

    G1 = testGraph[1]
    authorG1 = testGraph[0]

    groups = authorG1.split('_')
    authorG1 = groups[0] 
    booksNumber = [0 for x in range(len(authors))]
    nodeWeights = [0 for x in range(len(authors))]

    for authorG2 in trainGraph.keys():

        authorindex = authors.index(authorG2)
        G2= trainGraph[authorG2]
        maxEdge = 0
        maxNode = 0

        #Searching max weight
        for v1, v2, _ in G1.edges(data=True) :
            if(G2.has_edge(v1,v2)):
                if(G2[v1][v2]["weight"] > maxEdge):
                    maxEdge = G2[v1][v2]["weight"]
        for v in G1:
            if(G2.has_node(v)):
                if(G2.node[v]['weight']  > maxNode):
                    maxNode = G2.node[v]['weight'] 

        #Appliying a normalizer function 
        for v1, v2, _ in G1.edges(data=True) :
            if(G2.has_edge(v1,v2)):
                EdgesDict[(testGraph[0], authorG2)] +=func(G2[v1][v2]["weight"], f, maxEdge)
        for v in G1:
            if(G2.has_node(v)):
                NodesDict[(testGraph[0], authorG2)] +=func(G2.node[v]['weight'] ,f, maxNode)

    sorted_edges = sorted(EdgesDict.items(), key=itemgetter(1), reverse=True)
    sorted_nodes = sorted(NodesDict.items(), key=itemgetter(1), reverse=True)            

    return sorted_edges, sorted_nodes