
# coding: utf-8

# # Author Profiling based on Ngrams Graphs Validation

# Loading Libraries 

import sys
import time
import random
import os

from modules.utils import read_file,progress
from modules.graphs import *

from decimal import *

from collections import defaultdict
from datetime import datetime

import warnings
warnings.filterwarnings("ignore", category=UserWarning, module='bs4')

'''
import nltk

nltk.download('punkt')  
nltk.download('averaged_perceptron_tagger')    

'''
# Loading Global Variables
booksdir = ""

#Variables used in GraphGenerator
def initVariables(dirname, nTrain, nTest):

    global booksdir
    
    bookIndex =[]
    authors = set([])

    bookList = []
    booksTest = []
    booksTrain = []
    

    booksdir = str(dirname)+"/en/"
    folders = os.listdir(booksdir)
    prog= dict()
    
    for folder in folders:
        
        books = os.listdir(os.path.join(booksdir,folder))
        print('total books:'+ str(len(books)))
        prog['folders']= {'name':folder, 'done': 0, 'total':len(books)}
        for index,book in enumerate(books):
            bookDict= dict()
            if ".DS_Store" in books:
                books.remove(".DS_Store")

            data = book.replace(".xml", "").split("_")
            bookDict['name'] = book
            bookDict['author']= str(data[3])+"_"+str(data[2])
            authors.add(bookDict['author'])
            bookDict['folder']= booksdir+folder
            data,text = read_file(bookDict['folder']+'/'+bookDict['name'])
            
            bookDict['text'] = text
            bookDict['sex'] = data[0]
            bookDict['age'] = data[1]
            bookList.append(bookDict)
            prog['folders']['done']+=1
            a, b = divmod(index,10)
            if b==0:
                progress(prog)



    print("Total books: "+ str(len(books)))
    print("Total profiles: " +str(len(authors)))

    authors = list(authors)
    random.shuffle(bookList)

    bookSample = []
    for profile in authors:
        bookSample.append([profile, 0])

    bookIndex = [[] for x in range(int(len(bookSample)))] 
    for book in bookList:
        index = authors.index(book['author'])
        bookIndex[index].append(book)

    for i in range(len(bookIndex)):
        for j in range(int(nTrain)):
            booksTrain.append(bookIndex[i][j])    

    counter = 0
    for book in bookList:
        if counter < int(nTest):
            if book not in booksTrain:
                booksTest.append(book)
                counter += 1

    return booksTrain, booksTest

def initializeNTrain(books, number):

    authors = set([])
    trainBooks = []

    for book in books:
        bookName = book.replace(".xml", "")
        data = bookName.split("_")
        authors.add(str(data[2])+"_"+str(data[3]))
    
    for author in authors:
        counter = 0
        for book in books:
            if counter < number:
                if author in book:
                    trainBooks.append(book)
                    counter += 1
            else:
                break

    return trainBooks

def GraphBookGenerator(book,graphType,maxNgram,it):
    
    G=nx.DiGraph()
    counter = 0

    
    for index,text in enumerate(book['text']):
        
        tokens = tokenize_text(text) 
        author = "s"+book['sex']+"_a"+book['age']+str(index).zfill(3)
        for ngram in range(2, int(maxNgram)):
            path = os.path.join('results','graph','iter'+str(it),graphType,'ngram'+str(ngram))
            filename = os.path.join(path, author+"_"+str(index)+"_"+book['name']+".gexf")
            if not os.path.exists(path):
                os.makedirs(path)
            G = toGraph(tokens, G, ngram)
            nx.write_gexf(G, filename)
            G.clear()

def createBookGraph(books, graphType, it, maxNgram):

    print(str(len(books))+ " " + graphType + " " +str(it))
    total = len(books)
    for index,book in enumerate(books):
        print("loading: "+str(it)+"/"+str(total)+"\r", end="")
        GraphBookGenerator(book, graphType,maxNgram, it)



def create_folders():
    path = "results"
    if not os.path.exists(path):
        os.makedirs(path)
    if not os.path.exists(path+'/graph'):
        os.makedirs(path+'/graph')
    if not os.path.exists(path+'/graph/train'):
        os.makedirs(path+'/graph/train')
    if not os.path.exists(path+'/graph/test'):
        os.makedirs(path+'/graph/test')

def BookToGraph(itera, ngrams, booksTrain, booksTest):
    for it in range(1,int(itera)+1): #10
        createBookGraph(booksTrain, "train", it,ngrams)
        createBookGraph(booksTest , "test" , it,ngrams)

def main(argv):

    getcontext().prec = 2

    if(len(argv))<4:
        print("los argumentos deben ser: 'directorio', nº entrenamiento, nº prueba, nª ngramas")
        sys.exit(0)

    dirname = argv[0]
    nTrain = argv[1]
    nTest  = argv[2]
    ngrams  = argv[3]
    rand = argv[4]
    itera = argv[5]

    print(datetime.time(datetime.now()))
    random.seed(rand)

    create_folders()
    booksTrains, booksTest = initVariables(dirname, int(nTrain)*10, nTest)
    BookToGraph(itera, ngrams, booksTrains, booksTest)


if __name__ == "__main__":
   main(sys.argv[1:])