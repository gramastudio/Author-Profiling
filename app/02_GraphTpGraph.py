
# coding: utf-8

# # Author Profiling based on Ngrams Graphs Validation

# Loading Libraries 

import sys
import time
import random

from modules.utils import *
from modules.graphs import *

#from networkx_addon import *
from decimal import *

from datetime import datetime


def main(argv):

    getcontext().prec = 2

    if(len(argv))<2:
        print("los argumentos deben ser: 'nº entrenamiento, rand ")
        sys.exit(0)

    nTrain = argv[0]
    rand = argv[1]
    itera = argv[2]

    print(datetime.time(datetime.now()))
    random.seed(rand)

    for n in range(20, int(nTrain)*10, 20):
        SingleGraphToGraph(itera, n)


if __name__ == "__main__":
   main(sys.argv[1:])